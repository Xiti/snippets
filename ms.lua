
function time_ms(miliseconds)

	if not miliseconds then
		return ""
	end

	local seconds = math.floor(miliseconds/1000)

	local centiseconds = math.floor(math.fmod(miliseconds,1000))
	local minutes = math.floor(seconds/60)
	local seconds = math.fmod(seconds,60)
	do
		return string.format("%02d:%02d:%03d",minutes,seconds,centiseconds)
	end
end
